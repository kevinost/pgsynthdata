import utils
from typing import List, Any
import datetime



def generate_no_constraint(data_type: str, rows_to_gen: int, avg_width: int,
                           table_name: str, numeric_precision: int = None,
                           numeric_precision_radix: int = None, numeric_scale: int = None,
                           min_bound: Any = None, max_bound: Any = None,
                           list_for_bound: List[str] = None) -> List[str]:

    generated_vals = list()

    def numeric():
        for index in range(rows_to_gen):
            generated_vals.append(
                utils.random_number(numeric_precision, numeric_precision_radix,
                                    numeric_scale, min_value=min_bound, max_value=max_bound))

    def date():
        for index in range(rows_to_gen):
            if data_type in ('timestamp', 'timestamp without time zone'):
                generated_vals.append(utils.random_date(min_bound, max_bound, time=True))
            else:
                generated_vals.append(utils.random_date(min_bound, max_bound))

    def varchar():
        for index in range(rows_to_gen):
            generated_vals.append(utils.random_word(
                avg_width - 1,
                value=list_for_bound[utils.gen_random_number(0, len(list_for_bound) - 1)]))

    def no_type():
        print(
            f'The "{data_type}" data type is not supported. '
            f'Skipping the table\'s "{table_name}" data generation...')
        return

    funcDict = {'text': varchar,
                'character varying': varchar,
                'character': varchar,
                'smallint': numeric,
                'integer': numeric,
                'bigint': numeric,
                'decimal': numeric,
                'numeric': numeric,
                'date': date,
                'timestamp': date,
                'timestamp without time zone': date,
                'not_supported': no_type}

    funcDict[data_type]()

    return generated_vals


def generate_fk(data_type: str, rows_to_gen: int, table_name: str, column_name: str,
                pk_values, fk_ref_info, min_bound: Any = None,
                max_bound: Any = None) -> List[Any]:

    def numeric():
        counter = 0
        pk_values_list = pk_values[ref_table][ref_column]

        if rows_to_gen < len(pk_values_list):
            while len(generated_vals) < rows_to_gen:
                for value in pk_values_list:
                    if len(generated_vals) < rows_to_gen:
                        if counter < len(pk_values_list):
                            counter = counter + 1

                            if min_bound <= value <= max_bound and value not in generated_vals:
                                generated_vals.append(value)
                        else:
                            l_min_bound = min(pk_values_list)
                            l_max_bound = max(pk_values_list)
                            if l_min_bound <= value <= l_max_bound and value not in generated_vals:
                                generated_vals.append(value)

    def date():
        counter = 0
        pk_values_list = pk_values[ref_table][ref_column]
        while len(generated_vals) < rows_to_gen:  # ensure that all values are created
            for value in pk_values_list:
                if len(generated_vals) < rows_to_gen:  # ensure not to exceed values during for loop
                    if counter < len(pk_values_list):  # consider every value in PK_values once
                        counter = counter + 1
                        if min_bound <= value <= max_bound and value not in generated_vals:
                            generated_vals.append(value)
                    else:  # if pk_values does not contain enough values then extend boundaries
                        l_min_bound = min(pk_values_list)
                        l_max_bound = max(pk_values_list)
                        if l_min_bound <= value <= l_max_bound and value not in generated_vals:
                            generated_vals.append(value)

    def varchar():
        pk_values_list = pk_values[ref_table][ref_column]
        while len(generated_vals) < rows_to_gen:
            for value in pk_values_list:
                if len(generated_vals) < rows_to_gen:
                    generated_vals.append(value)

    def no_type():
        print(
            f'The "{data_type}" data type is not supported. '
            f'Skipping the table\'s "{table_name}" data generation...')
        return

    generated_vals = list()

    ref_table = fk_ref_info[column_name]['ref_table']
    ref_column = fk_ref_info[column_name]['ref_column']

    funcDict = {'text': varchar,
                'character varying': varchar,
                'character': varchar,
                'smallint': numeric,
                'integer': numeric,
                'bigint': numeric,
                'decimal': numeric,
                'numeric': numeric,
                'date': date,
                'timestamp': date,
                'timestamp without time zone': date,
                'not_supported': no_type}

    funcDict[data_type]()

    return generated_vals


def generate_pk(data_type: str, rows_to_gen: int, avg_width: int, table_name: str,
                min_value: Any = None, max_value: Any = None,
                list_for_bound: List[str] = None) -> List[Any]:

    generated_vals = list()

    def numeric():
        nonlocal max_value
        if rows_to_gen > (max_value - min_value):
            max_value = min_value + rows_to_gen
        try:
            nonlocal generated_vals
            generated_vals = utils.random.sample(range(min_value, max_value), int(rows_to_gen))
        except ValueError:
            print('Sample size exceeded population size.')

    def date():
        nonlocal max_value
        # ensure that enough dates btw. start and end are available
        days_between = utils.days_between(min_value, max_value)
        if rows_to_gen > days_between:
            max_value = min_value + datetime.timedelta(days=rows_to_gen)

        while len(generated_vals) < rows_to_gen:
            for index in range(rows_to_gen):
                if data_type in ('timestamp', 'timestamp without time zone'):
                    value = utils.random_date(min_value, max_value, time=True)
                    if value not in generated_vals:
                        generated_vals.append(value)
                else:
                    value = utils.random_date(min_value, max_value)
                    if value not in generated_vals:
                        generated_vals.append(value)

    def varchar():
        for index in range(rows_to_gen):
            value = utils.random_word(avg_width - 1,
                                      value=list_for_bound[utils.gen_random_number(0, len(list_for_bound) - 1)])
            if value not in generated_vals:
                generated_vals.append(value)

    def no_type():
        print(
            f'The "{data_type}" data type is not supported. '
            f'Skipping the table\'s "{table_name}" data generation...')
        return

    funcDict = {'text': varchar,
                'character varying': varchar,
                'character': varchar,
                'smallint': numeric,
                'integer': numeric,
                'bigint': numeric,
                'decimal': numeric,
                'numeric': numeric,
                'date': date,
                'timestamp': date,
                'timestamp without time zone': date,
                'not_supported': no_type}

    funcDict[data_type]()

    return generated_vals
